#!/bin/bush
#set up Host name
hostnamectl set-hostname "web.srikanth.org"

#configure host into host file
echo "'hostname -I | awk '{print $1}','hostname'" >> /etc/hosts

#updating the ubuntu local repository with online repository "package manager command"
sudo apt-get update

#download install configure utility softwares
sudo apt-get install git curl unzip tree wget -y

#To Restart SSM agent on linux_Ubuntu


#associate IAM user peofile to instance

#aws ec2 associate-iam-instance-profile --iam-instance-profile Name=ec2connect --instance-id ""

#To update APT repository
sudo apt-getsudo systemctl restart snap.amazon-ssm-agent.amazon-ssm-agent.service update

#To install Apache2 software
sudo apt-get install apache2

#To create a own website
sudo mkdir /var/www/srikanth/

#virtual host configuration
cd/var/www/srikanth/

touch webserver.html

echo "<html>" >> /var/www/srikanth/webserver.html
echo "<head>" >> /var/www/srikanth/webserver.html
echo "  <title> Ubuntu rocks! </title>" >> /var/www/srikanth/webserver.html
echo "</head>" >> /var/www/srikanth/webserver.html
echo "<body>" >> /var/www/srikanth/webserver.html
echo "  <p> I'm running this website on an Ubuntu Server server!" >> /var/www/srikanth/webserver.html
echo "</body>" >> /var/www/srikanth/webserver.html
echo "</html>" >> /var/www/srikanth/webserver.html

# Setting up the VirtualHost Configuration File
cd /etc/apache2/sites-available/

# Copy default VirtualHost file
sudo cp 000-default.conf srikanth.conf

# sudo vi srikanth.conf
# ServerAdmin yourname@example.com
# DocumentRoot /var/www/srikanth/
# ServerName gci.example.com

# Activating VirtualHost file
sudo a2ensite srikanth.conf

# restart Apache by typing:
service apache2 reload
