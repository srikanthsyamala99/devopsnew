#!/bin/bash

# Setup Hostname 
hostnamectl set-hostname "wb.ecloudbinary.io"

# Configure Hostname unto hosts file 
echo "`hostname -I | awk '{ print $1}'` `hostname`" >> /etc/hosts 

# Update the Ubuntu Local Repository with Online Repository 
sudo apt-get update 

# Download, Install & Configure Utility Softwares 
sudo apt-get install git curl unzip tree wget -y 

# To Restart SSM Agent on Ubuntu 
sudo systemctl restart snap.amazon-ssm-agent.amazon-ssm-agent.service

# Attach Instance profile To EC2 Instance 
# aws ec2 associate-iam-instance-profile --iam-instance-profile Name=ec2Connect --instance-id i-087b048b43205d3d4 --profile devops

# Installing Apache - Webserver

# Update the APT Repository
sudo apt-get update

# Download Install & Configure Apache WebServer i.e. Apache2 
sudo apt-get install apache2 -y 

# Creating Your Own Website
sudo mkdir /var/www/gci/

# virtual hosts configuration file
cd /var/www/gci/

touch index.html

echo "<html>" >> /var/www/gci/index.html
echo "<head>" >> /var/www/gci/index.html
echo "  <title> Ubuntu rocks! </title>" >> /var/www/gci/index.html
echo "</head>" >> /var/www/gci/index.html
echo "<body>" >> /var/www/gci/index.html
echo "  <p> I'm running this website on an Ubuntu Server server!" >> /var/www/gci/index.html
echo "</body>" >> /var/www/gci/index.html
echo "</html>" >> /var/www/gci/index.html

# Setting up the VirtualHost Configuration File
cd /etc/apache2/sites-available/

# Copy default VirtualHost file
sudo cp 000-default.conf gci.conf

# sudo vi gci.conf
# ServerAdmin yourname@example.com
# DocumentRoot /var/www/gci/
# ServerName gci.example.com

# Activating VirtualHost file
sudo a2ensite gci.conf

# restart Apache by typing:
service apache2 reload
