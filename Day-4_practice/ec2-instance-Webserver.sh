# Launch AWS EC2 Instance Of Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-0756a1c858554433e" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-04c4fc8ebda7e8edf" \
--security-group-ids "sg-0088cfc874a4223bb" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Apache2_webserver},{Key=Environment,Value=Dev}]' \
--key-name "aws-mumbai-keys"
#--user-data file://install-utilities.txt