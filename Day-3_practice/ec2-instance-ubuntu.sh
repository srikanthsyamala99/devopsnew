# Launch AWS EC2 Instance Of Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-05ba3a39a75be1ec4" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-07083542675b6eca8" \
--security-group-ids "sg-09a99663a9521b3c5" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Ubuntu_Linux},{Key=Environment,Value=Dev}]' \
--key-name "aws-mumbai-keys" \
--user-data file://install-utilities.txt
