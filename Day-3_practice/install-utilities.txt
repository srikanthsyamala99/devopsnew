#!/bin/bush
#set up Host name
hostnamectl set-hostname "web.srikanth.org" 

#configure host into host file
echo "'hostname -I |awk '{print $1}','hostname'" >> /etc/hosts
#updating the ubuntu local repository with online repository "package manager command"
sudo apt-get update

#download install configure utility softwares
sudo apt-get install git curl unzip tree wget -y

#To Restart SSM agent on linux_Ubuntu
sudo systemctl restart snap.amazon-ssm-agent.amazon-ssm-agent.service

#associate IAM user peofile to instance

#aws ec2 associate-iam-instance-profile --iam-instance-profile Name=ec2connect --instance-id ""
